﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CONTPAQI.Entities;

namespace CONTPAQI.Services.SOAP
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ICONTPAQIService
    {
        [OperationContract]
        [FaultContract(typeof(Response))]
        List<Empresa> ObtenerEmpresas(int? idEmpresa);

        [OperationContract]
        [FaultContract(typeof(Response))]
        void GrabarEmpresa(Empresa empresa);
    }
}
