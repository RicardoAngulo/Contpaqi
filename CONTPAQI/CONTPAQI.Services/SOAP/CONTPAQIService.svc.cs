﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CONTPAQI.BusinessLogic;
using CONTPAQI.Entities;

namespace CONTPAQI.Services.SOAP
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class CONTPAQIService : ICONTPAQIService
    {
        public List<Empresa> ObtenerEmpresas(int? idEmpresa)
        {
            try
            {
                return new EmpresaBL().ObtenerEmpresas(idEmpresa);
            }
            catch (Exception exception)
            {
                var response = new Response();
                response.Message = exception.Message;
                response.StackTrace = exception.StackTrace;
                response.Success = false;
                throw new FaultException<Response>(response);
            }
        }

        public void GrabarEmpresa(Empresa empresa)
        {
            try
            {
                new EmpresaBL().GrabarEmpresa(empresa);
            }
            catch (Exception exception)
            {
                var response = new Response();
                response.Message = exception.Message;
                response.StackTrace = exception.StackTrace;
                response.Success = false;
                throw new FaultException<Response>(response);
            }
        }
    }
}
