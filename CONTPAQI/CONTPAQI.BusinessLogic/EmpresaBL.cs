﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CONTPAQI.DataAccess;
using CONTPAQI.Entities;

namespace CONTPAQI.BusinessLogic
{
    public class EmpresaBL : IEmpresaBL
    {
        public EmpresaBL() { }

        public List<Empresa> ObtenerEmpresas(int? idEmpresa)
        {
            try
            {
                using (var db = new CONTPAQIEntities())
                {
                    var empresas = db.Empresas
                        .Where(x => x.IDEmpresa == (idEmpresa ?? x.IDEmpresa))
                        .ToList();

                    return empresas.Select(x => new Empresa
                    {
                        IDEmpresa = x.IDEmpresa,
                        RazonSocial = x.RazonSocial.DesEncriptar(),
                        RFC = x.RFC.DesEncriptar(),
                        Pais = x.Pais.DesEncriptar(),
                        Estado = x.Estado.DesEncriptar(),
                        Municipio = x.Municipio.DesEncriptar(),
                        Calle = x.Calle.DesEncriptar(),
                        NumeroInterior = x.NumeroInterior.DesEncriptar(),
                        NumeroExterior = x.NumeroExterior.DesEncriptar(),
                        Colonia = x.Colonia.DesEncriptar(),
                        Activo = x.Activo,
                        Borrado = x.Borrado,
                        FechaCreacion = x.FechaCreacion,
                        UsuarioCreador = x.UsuarioCreador.DesEncriptar(),
                        FechaUltAct = x.FechaUltAct,
                        UsuarioUltAct = !string.IsNullOrEmpty(x.UsuarioUltAct) ? x.UsuarioUltAct.DesEncriptar() : ""
                    }).ToList();
                }
            }
            catch (EntityException entityException)
            {
                throw new Exception(entityException.InnerException.Message, entityException);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void GrabarEmpresa(Empresa empresa)
        {
            try
            {
                using (var db = new CONTPAQIEntities())
                {
                    var emp = db.Empresas.FirstOrDefault(x => x.IDEmpresa.Equals(empresa.IDEmpresa));
                    if (emp != null)
                    {
                        emp.RazonSocial = empresa.RazonSocial.Encriptar();
                        emp.RFC = empresa.RFC.Encriptar();
                        emp.Pais = empresa.Pais.Encriptar();
                        emp.Estado = empresa.Estado.Encriptar();
                        emp.Municipio = empresa.Municipio.Encriptar();
                        emp.Calle = empresa.Calle.Encriptar();
                        emp.NumeroInterior = !string.IsNullOrEmpty(empresa.NumeroInterior) ? empresa.NumeroInterior.Encriptar() : "";
                        emp.NumeroExterior = empresa.NumeroExterior.Encriptar();
                        emp.Colonia = empresa.Colonia.Encriptar();
                        emp.Activo = empresa.Activo;
                        emp.Borrado = empresa.Borrado;
                        emp.FechaUltAct = empresa.FechaUltAct;
                        emp.UsuarioUltAct = empresa.UsuarioUltAct.Encriptar();
                        db.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        emp = new Empresas();
                        emp.RazonSocial = empresa.RazonSocial.Encriptar();
                        emp.RFC = empresa.RFC.Encriptar();
                        emp.Pais = empresa.Pais.Encriptar();
                        emp.Estado = empresa.Estado.Encriptar();
                        emp.Municipio = empresa.Municipio.Encriptar();
                        emp.Calle = empresa.Calle.Encriptar();
                        emp.NumeroInterior = !string.IsNullOrEmpty(empresa.NumeroInterior) ? empresa.NumeroInterior.Encriptar() : "";
                        emp.NumeroExterior = empresa.NumeroExterior.Encriptar();
                        emp.Colonia = empresa.Colonia.Encriptar();
                        emp.Activo = empresa.Activo;
                        emp.Borrado = empresa.Borrado;
                        emp.FechaCreacion = empresa.FechaCreacion;
                        emp.UsuarioCreador = empresa.UsuarioCreador.Encriptar();
                        db.Entry(emp).State = System.Data.Entity.EntityState.Added;
                    }

                    db.SaveChanges();
                }
            }
            catch (EntityException entityException)
            {
                throw new Exception(entityException.InnerException.Message, entityException);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void EliminarEmpresa(Empresa empresa)
        {
            try
            {
                using (var db = new CONTPAQIEntities())
                {
                    var emp = db.Empresas.FirstOrDefault(x => x.IDEmpresa.Equals(empresa.IDEmpresa));
                    if (emp != null)
                    {
                        emp.Activo = empresa.Activo;
                        emp.Borrado = empresa.Borrado;
                        emp.FechaUltAct = empresa.FechaUltAct;
                        emp.UsuarioUltAct = empresa.UsuarioUltAct.Encriptar();
                        db.Entry(emp).State = System.Data.Entity.EntityState.Modified;

                        db.SaveChanges();
                    }
                }
            }
            catch (EntityException entityException)
            {
                throw new Exception(entityException.InnerException.Message, entityException);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}