﻿using CONTPAQI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CONTPAQI.BusinessLogic
{
    public interface IEmpresaBL
    {
        List<Empresa> ObtenerEmpresas(int? idEmpresa);

        void GrabarEmpresa(Empresa empresa);

        void EliminarEmpresa(Empresa empresa);
    }
}
