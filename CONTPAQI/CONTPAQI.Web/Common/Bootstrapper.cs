﻿using CONTPAQI.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace CONTPAQI.Web.Common
{
    public static class Bootstrapper
    {
        public static void Initialize()
        {
            var container = new UnityContainer();
            container.RegisterType<IEmpresaBL, EmpresaBL>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}