﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;

namespace CONTPAQI.Web.Common
{
    public class Logs
    {
        public static ILog GetLogger()
        {
            log4net.Config.XmlConfigurator.Configure();
            return LogManager.GetLogger(Assembly.GetExecutingAssembly().GetTypes().First());
        }
    }
}