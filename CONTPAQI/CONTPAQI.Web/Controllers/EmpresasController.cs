﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using CONTPAQI.BusinessLogic;
using CONTPAQI.Entities;
using CONTPAQI.Web.Common;
using log4net;

namespace CONTPAQI.Web.Models
{
    public class EmpresasController : Controller
    {
        private static readonly log4net.ILog _log = Logs.GetLogger();
        private readonly IEmpresaBL _empresaBL;

        public EmpresasController(IEmpresaBL empresaBL)
        {
            _empresaBL = empresaBL;
        }

        // GET: Empresas
        public ActionResult Index()
        {
            //var empresas = new EmpresaBL().ObtenerEmpresas(null);
            var empresas = _empresaBL.ObtenerEmpresas(null);
            return View(empresas);
        }

        // GET: Empresas/Create
        public ActionResult Create()
        {
            return View(new Empresa());
        }

        // POST: Empresas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Empresa empresa)
        {
            try
            {
                //throw new Exception("Error forzado!");
                if (ModelState.IsValid)
                {
                    empresa.IDEmpresa = 0;
                    empresa.Activo = true;
                    empresa.Borrado = false;
                    empresa.FechaCreacion = DateTime.Now;
                    empresa.UsuarioCreador = "sistemas";

                    //new EmpresaBL().GrabarEmpresa(empresa);
                    _empresaBL.GrabarEmpresa(empresa);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception exception)
            {
                _log.Error(exception.Message);
                ModelState.AddModelError("", "La empresa no se ha guardado, favor de revisarlo con su administrador de sistemas.");
            }

            return View(empresa);
        }

        // GET: Empresas/Edit/5
        public ActionResult Edit(int idEmpresa)
        {
            //var empresa = new EmpresaBL().ObtenerEmpresas(idEmpresa).FirstOrDefault();
            var empresa = _empresaBL.ObtenerEmpresas(idEmpresa).FirstOrDefault();
            return View(empresa);
        }

        // POST: Empresas/Edit/5
        [HttpPost]
        public ActionResult Edit(int idEmpresa, Empresa empresa)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    empresa.FechaUltAct = DateTime.Now;
                    empresa.UsuarioUltAct = "sistemas";

                    //new EmpresaBL().GrabarEmpresa(empresa);
                    _empresaBL.GrabarEmpresa(empresa);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception exception)
            {
                _log.Error(exception.Message);
                ModelState.AddModelError("", "La empresa no se ha actualizado, favor de revisarlo con su administrador de sistemas.");
            }

            return View(empresa);
        }

        // GET: Empresas/Delete/5
        public ActionResult Delete(int idEmpresa)
        {
            //var empresa = new EmpresaBL().ObtenerEmpresas(idEmpresa).FirstOrDefault();
            var empresa = _empresaBL.ObtenerEmpresas(idEmpresa).FirstOrDefault();
            return View(empresa);
        }

        // POST: Empresas/Delete/5
        [HttpPost]
        public ActionResult Delete(int idEmpresa, Empresa empresa)
        {
            try
            {
                empresa.Activo = false;
                empresa.Borrado = true;
                empresa.FechaUltAct = DateTime.Now;
                empresa.UsuarioUltAct = "sistemas";

                //new EmpresaBL().EliminarEmpresa(empresa);
                _empresaBL.EliminarEmpresa(empresa);
                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                _log.Error(exception.Message);
                ModelState.AddModelError("", "La empresa no se ha eliminado, favor de revisarlo con su administrador de sistemas.");
            }

            return View(empresa);
        }
    }
}
