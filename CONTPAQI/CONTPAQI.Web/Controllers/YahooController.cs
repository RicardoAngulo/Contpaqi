﻿using CONTPAQI.Entities;
using CONTPAQI.Web.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CONTPAQI.Web.Controllers
{
    public class YahooController : Controller
    {
        private static readonly log4net.ILog _log = Logs.GetLogger();
        private readonly string _apiRESTYahoo = ConfigurationManager.AppSettings["APIRestYahoo"];
        private readonly string _tokenYahoo = ConfigurationManager.AppSettings["TokenYahoo"];

        // GET: Yahoo
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public async Task<ActionResult> Index()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_apiRESTYahoo);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("x-api-key", _tokenYahoo);
                    HttpResponseMessage response = await client.GetAsync("/v6/finance/quote?region=US&lang=en&symbols=AAPL%2CBTC-USD%2CEURUSD%3DX");
                    
                    var json = response.Content.ReadAsStringAsync().Result;
                    var quote = JsonConvert.DeserializeObject<Root>(json).QuoteResponse.Quote;
                    return View(quote);
                }
            }
            catch (Exception exception)
            {
                _log.Error(exception.Message);
                return View();
            }
        }
    }
}
