﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CONTPAQI.Entities
{
    [DataContract(Name = "EntidadBase", Namespace = "CONTPAQI.Entities")]
    public class EntidadBase
    {
        [DataMember]
        public bool Activo { get; set; }
        [DataMember]
        public bool Borrado { get; set; }
        [DataMember]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha Creación")]
        public DateTime FechaCreacion { get; set; }
        [DataMember]
        [Display(Name = "Usuario Creadors")]
        public string UsuarioCreador { get; set; }
        [DataMember]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha Ult. Act.")]
        public DateTime? FechaUltAct { get; set; }
        [DataMember]
        [Display(Name = "Usuario Ult. Act.")]
        public string UsuarioUltAct { get; set; }

        public EntidadBase()
        {
            Activo = false;
            Borrado = false;
            FechaCreacion = DateTime.MinValue;
            UsuarioCreador = string.Empty;
            FechaUltAct = DateTime.MinValue;
            UsuarioUltAct = string.Empty;
        }
    }
}
