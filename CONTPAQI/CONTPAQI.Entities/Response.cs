﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CONTPAQI.Entities
{
    [DataContract(Name = "Response", Namespace = "CONTPAQI.Entities")]
    public class Response
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string StackTrace { get; set; }
        [DataMember]
        public int Severity { get; set; }
        [DataMember]
        public List<Object> Data { get; set; }
        [DataMember]
        public bool Success { get; set; }

        public Response()
        {
            Message = string.Empty;
            StackTrace = string.Empty;
            Severity = 0;
            Data = new List<object>();
            Success = false;
        }
    }
}
