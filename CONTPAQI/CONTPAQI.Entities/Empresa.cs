﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CONTPAQI.Entities
{
    [DataContract(Name = "Empresa", Namespace = "CONTPAQI.Entities")]
    public class Empresa : EntidadBase
    {
        [DataMember]
        public int IDEmpresa { get; set; }
        [DataMember]
        [Required]
        [StringLength(250)]
        [Display(Name = "Razón Social")]
        public string RazonSocial { get; set; }
        [DataMember]
        [Required]
        [StringLength(13)]
        [MaxLength(13, ErrorMessage = "La longitud maxima del RFC es de 13 caracteres")]
        public string RFC { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string Pais { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string Estado { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string Municipio { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string Calle { get; set; }
        [DataMember]
        [StringLength(50)]
        [Display(Name = "Núm. Interior")]
        public string NumeroInterior { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        [Display(Name = "Núm. Exterior")]
        public string NumeroExterior { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string Colonia { get; set; }

        public Empresa()
        {
            IDEmpresa = 0;
            RazonSocial = string.Empty;
            RFC = string.Empty;
            Pais = string.Empty;
            Estado = string.Empty;
            Municipio = string.Empty;
            Calle = string.Empty;
            NumeroInterior = string.Empty;
            NumeroExterior = string.Empty;
            Colonia = string.Empty;
        }
    }
}
