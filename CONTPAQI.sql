USE [master]
GO
/****** Object:  Database [CONTPAQI]    Script Date: 26/01/2022 10:45:52 p. m. ******/
CREATE DATABASE [CONTPAQI]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CONTPAQI', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\CONTPAQI.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CONTPAQI_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\CONTPAQI_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [CONTPAQI] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CONTPAQI].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CONTPAQI] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CONTPAQI] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CONTPAQI] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CONTPAQI] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CONTPAQI] SET ARITHABORT OFF 
GO
ALTER DATABASE [CONTPAQI] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CONTPAQI] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CONTPAQI] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CONTPAQI] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CONTPAQI] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CONTPAQI] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CONTPAQI] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CONTPAQI] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CONTPAQI] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CONTPAQI] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CONTPAQI] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CONTPAQI] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CONTPAQI] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CONTPAQI] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CONTPAQI] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CONTPAQI] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CONTPAQI] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CONTPAQI] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CONTPAQI] SET  MULTI_USER 
GO
ALTER DATABASE [CONTPAQI] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CONTPAQI] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CONTPAQI] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CONTPAQI] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CONTPAQI] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CONTPAQI] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [CONTPAQI] SET QUERY_STORE = OFF
GO
USE [CONTPAQI]
GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 26/01/2022 10:45:52 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresas](
	[IDEmpresa] [int] IDENTITY(1,1) NOT NULL,
	[RazonSocial] [varchar](250) NOT NULL,
	[RFC] [varchar](250) NOT NULL,
	[Pais] [varchar](250) NOT NULL,
	[Estado] [varchar](250) NOT NULL,
	[Municipio] [varchar](250) NOT NULL,
	[Calle] [varchar](250) NOT NULL,
	[NumeroInterior] [varchar](250) NOT NULL,
	[NumeroExterior] [varchar](250) NOT NULL,
	[Colonia] [varchar](250) NOT NULL,
	[Activo] [bit] NOT NULL,
	[Borrado] [bit] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[UsuarioCreador] [varchar](50) NOT NULL,
	[FechaUltAct] [datetime] NULL,
	[UsuarioUltAct] [varchar](50) NULL,
 CONSTRAINT [PK_Empresas] PRIMARY KEY CLUSTERED 
(
	[IDEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_RazonSocial]  DEFAULT ('') FOR [RazonSocial]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_RFC]  DEFAULT ('') FOR [RFC]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_Pais]  DEFAULT ('') FOR [Pais]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_Estado]  DEFAULT ('') FOR [Estado]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_Municipio]  DEFAULT ('') FOR [Municipio]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_Calle]  DEFAULT ('') FOR [Calle]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_NumeroInterior]  DEFAULT ('') FOR [NumeroInterior]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_NumeroExterior]  DEFAULT ('') FOR [NumeroExterior]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_Colonia]  DEFAULT ('') FOR [Colonia]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_Activo]  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_Borrado]  DEFAULT ((0)) FOR [Borrado]
GO
ALTER TABLE [dbo].[Empresas] ADD  CONSTRAINT [DF_Empresas_FechaCreacion]  DEFAULT (getdate()) FOR [FechaCreacion]
GO
USE [master]
GO
ALTER DATABASE [CONTPAQI] SET  READ_WRITE 
GO
